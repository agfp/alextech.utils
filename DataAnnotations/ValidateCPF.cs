using System.ComponentModel.DataAnnotations;

namespace AlexTech.Utils.DataAnnotations
{
    public class ValidateCPF : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (Validators.IsCPF(value.ToString()))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("CPF Inválido");
        }
    }

}