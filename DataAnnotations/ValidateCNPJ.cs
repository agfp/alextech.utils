using System.ComponentModel.DataAnnotations;

namespace AlexTech.Utils.DataAnnotations
{
    public class ValidateCNPJ : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (Validators.IsCNPJ(value.ToString()))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult("CNPJ Inválido");
        }
    }

}