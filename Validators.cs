using System.Text.RegularExpressions;
using System.Linq;

namespace AlexTech.Utils
{
    public static class Validators
    {
        private static string RemoveNonNumbers(string text)
        {
            var reg = new Regex(@"[^0-9]");
            var ret = reg.Replace(text, string.Empty);
            return ret;
        }

        public static bool IsCPF(string cpf)
        {
            var match1 = Regex.Match(cpf, @"^\d{11}|((\d{3}\.){2}\d{3}-\d{2})$");
            if (!match1.Success) return false;

            cpf = RemoveNonNumbers(cpf);

            var match2 = Regex.Match(cpf, @"^(.)(\1)*$");
            if (match2.Success) return false;
            if (cpf == "12345678909") return false;

            var numeros = cpf.ToCharArray()
                .Select(c => c - '0')
                .ToArray();

            var soma1 = numeros.Take(9)
                .Select((num, index) => (10 - index) * num)
                .Sum();
            var dv1 = soma1 % 11;
            dv1 = dv1 < 2 ? 0 : 11 - dv1;
            if (numeros[9] != dv1) return false;

            var soma2 = numeros.Take(10)
                .Select((num, index) => (11 - index) * num)
                .Sum();
            var dv2 = soma2 % 11;
            dv2 = dv2 < 2 ? 0 : 11 - dv2;

            return numeros[10] == dv2;
        }

        public static bool IsCNPJ(string cnpj)
        {
            var match1 = Regex.Match(cnpj, @"^\d{14}|(\d{2}\.\d{3}\.\d{3}/\d{4}-\d{2})$");
            if (!match1.Success) return false;

            cnpj = RemoveNonNumbers(cnpj);

            var match2 = Regex.Match(cnpj, @"^(.)(\1)*$");
            if (match2.Success) return false;

            var numeros = cnpj.ToCharArray()
               .Select(c => c - '0')
               .ToArray();

            var mt1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            var soma1 = numeros.Take(12)
                .Select((num, index) => num * mt1[index])
                .Sum();
            var dv1 = soma1 % 11;
            dv1 = dv1 < 2 ? 0 : 11 - dv1;
            if (numeros[12] != dv1) return false;

            var mt2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            var soma2 = numeros.Take(13)
               .Select((num, index) => num * mt2[index])
               .Sum();
            var dv2 = soma2 % 11;
            dv2 = dv2 < 2 ? 0 : 11 - dv2;

            return numeros[13] == dv2;
        }
    }
}
